==================
RTD Control System
==================

rtd_control_system provides a graphical user interface for the residence time distribution experiment at the felleslab, NTNU

To start the application::

    #!/usr/bin/env python

    from rtd_control_system import controller_app

    app = controller_app()
    app.run()


Installation
============

using pip::

	pip install -e git+https://bitbucket.org/sysengntnu/the-new-rtd-control-system#egg=rtd_control_system
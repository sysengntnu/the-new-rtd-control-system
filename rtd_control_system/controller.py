
import wx
import time
from model import Model 
from view_pump import ViewPump
from view_multiple_measurement import ViewMultipleMeasuremnt
from view_sample_controller import ViewSampleController
from pubsub import pub
from view_strip_chart import ViewStripChart
import ConfigParser

# Loads position of views
window_position_config = ConfigParser.ConfigParser()
window_position_config.read("./window_position.init")


# TODO: add these settings to a configuration file

PUMPS = dict()
PUMPS['pump_1'] = 'Pump No.: 1'
PUMPS['pump_2'] = 'Pump No.: 2'

TEMP_SENSORS = list()
TEMP_SENSORS.append('temperature_1')
TEMP_SENSORS.append('temperature_2')
TEMP_SENSORS.append('temperature_3')
TEMP_SENSORS.append('temperature_4')
TEMP_SENSORS.append('temperature_5')
TEMP_SENSORS.append('temperature_6')
TEMP_SENSORS.append('temperature_7')
TEMP_SENSORS.append('temperature_8')
TEMP_SENSORS.append('temperature_9')

TEMP_SENSOR_LABEL = dict()
TEMP_SENSOR_LABEL['temperature_1'] = dict()
TEMP_SENSOR_LABEL['temperature_2'] = dict()
TEMP_SENSOR_LABEL['temperature_3'] = dict()
TEMP_SENSOR_LABEL['temperature_4'] = dict()
TEMP_SENSOR_LABEL['temperature_5'] = dict()
TEMP_SENSOR_LABEL['temperature_6'] = dict()
TEMP_SENSOR_LABEL['temperature_7'] = dict()
TEMP_SENSOR_LABEL['temperature_8'] = dict()
TEMP_SENSOR_LABEL['temperature_9'] = dict()

TEMP_SENSOR_LABEL['temperature_1']['label'] = 'Sensor 1'
TEMP_SENSOR_LABEL['temperature_2']['label'] = 'Sensor 2'
TEMP_SENSOR_LABEL['temperature_3']['label'] = 'Sensor 3'
TEMP_SENSOR_LABEL['temperature_4']['label'] = 'Sensor 4'
TEMP_SENSOR_LABEL['temperature_5']['label'] = 'Sensor 5'
TEMP_SENSOR_LABEL['temperature_6']['label'] = 'Sensor 6'
TEMP_SENSOR_LABEL['temperature_7']['label'] = 'Sensor 7'
TEMP_SENSOR_LABEL['temperature_8']['label'] = 'Sensor 8'
TEMP_SENSOR_LABEL['temperature_9']['label'] = 'Sensor 9'

TEMP_SENSOR_LABEL['temperature_1']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_2']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_3']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_4']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_5']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_6']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_7']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_8']['unit'] = ' degC'
TEMP_SENSOR_LABEL['temperature_9']['unit'] = ' degC'

VOLTAGE_SENSORS = list()
VOLTAGE_SENSORS.append('voltage_0')
VOLTAGE_SENSORS.append('voltage_1')

VOLTAGE_SENSOR_LABEL = dict()
VOLTAGE_SENSOR_LABEL['voltage_0'] = dict()
VOLTAGE_SENSOR_LABEL['voltage_1'] = dict()

VOLTAGE_SENSOR_LABEL['voltage_0']['label'] = 'Voltage In'
VOLTAGE_SENSOR_LABEL['voltage_1']['label'] = 'Voltage out'

VOLTAGE_SENSOR_LABEL['voltage_0']['unit'] = ' '
VOLTAGE_SENSOR_LABEL['voltage_1']['unit'] = ' '


class Controller(wx.Frame):
	def __init__(self, app):


		# importing the model
		self.model = Model(sampling_rate=1)
		
		# initializing the view
		self.view = dict()

		self.view['sample_controller'] = ViewSampleController(None)
		self.view['sample_controller'].set_sampling_rate(1.0)

		self.view['sample_controller'].on.Bind(wx.EVT_TOGGLEBUTTON, self.start_sampling)  # When on button is pushed
		self.view['sample_controller'].off.Bind(wx.EVT_TOGGLEBUTTON, self.stop_sampling)  # When off button is pushed
		self.view['sample_controller'].spin_sampling_rate.Bind(wx.EVT_SPINCTRLDOUBLE, self.set_sampling_rate)  # When setpoint is changed

		# setting up pump views
		for pump in PUMPS.keys():
			self.view[pump] = ViewPump(None, secondary_title=PUMPS[pump], name=pump )

			# adding event binders
			self.view[pump].on.Bind(wx.EVT_TOGGLEBUTTON, self.turn_on_pump)  # When on button is pushed
			self.view[pump].off.Bind(wx.EVT_TOGGLEBUTTON, self.turn_off_pump)  # When off button is pushed
			self.view[pump].spin_setpoint.Bind(wx.EVT_SPINCTRLDOUBLE, self.set_setpoint)  # When setpoint is changed
			self.view[pump].Bind(wx.EVT_CLOSE, self.on_close)  # When the widow is closed

		# setting up view for the temperature readings
		self.view['temperature'] = ViewMultipleMeasuremnt(None, TEMP_SENSORS, TEMP_SENSOR_LABEL, secondary_title='Temperature Sensors')

		# setting up view for the voltage readings
		self.view['voltage'] = ViewMultipleMeasuremnt(None, VOLTAGE_SENSORS, VOLTAGE_SENSOR_LABEL, secondary_title='Voltage Sensors')


		# setting up plot
		self.view['plot'] = ViewStripChart(None, plot_fields=TEMP_SENSORS, xlabel='Time [s]', ylabel='Temperatures [degC]', ymax=30.)
		# plotting option: to reduce computation time, set to false 
		self.plot_relative_time = False

		for section in window_position_config.sections():
			x_pos = window_position_config.getint(section, 'x_position')
			y_pos = window_position_config.getint(section, 'y_position')
			pos  = x_pos, y_pos
			self.view[section].SetPosition(pos)
		
		for view in self.view:
			self.view[view].Bind(wx.EVT_CLOSE, self.on_close)  # When the widow is closed
			self.view[view].Show()


		# subscribing to signals from the model
		pub.subscribe(self.update_measurement_gui,'sampled')
		pub.subscribe(self.update_setpoint_gui,'pump_setpoint_changed')
		pub.subscribe(self.update_on_off_gui,'pump_status_changed')
		pub.subscribe(self.update_start_stop_gui,'sampling_status_changed')

		# starting the sampling, should be the last thing to be done in the initialization
		#self.model.start_sampling()
		
	def update_measurement_gui(self, measurements):
		for measurement in PUMPS:
			self.view[measurement].set_speed(measurements[measurement])

		self.view['voltage'].set_sensor_reading('voltage_0',measurements['voltage_0'])
		self.view['voltage'].set_sensor_reading('voltage_1',measurements['voltage_1'])

		for sensor in TEMP_SENSORS:
			self.view['temperature'].set_sensor_reading(sensor, measurements[sensor])

		time_since_start = self.model.data.data['time_since_start']

		if self.plot_relative_time:
			last_time = self.model.data.data['time_since_start'][-1]
			time_since_start[:] = [x-last_time for x in time_since_start]

		self.view['plot'].update_plot(time_since_start, self.model.data.data)


	def update_setpoint_gui(self, pump_name, setpoint_speed):
		self.view[pump_name].set_speed_setpoint(setpoint_speed)

	def update_on_off_gui(self, pump_name, pump_status):
		if pump_status == 'on':
			self.view[pump_name].toggle_on()
		elif pump_status == 'off':
			self.view[pump_name].toggle_off()

	def update_start_stop_gui(self, sampling_status):
		if sampling_status == 'started':
			self.view['sample_controller'].toggle_on()
		elif sampling_status == 'stopped':
			self.view['sample_controller'].toggle_off()

	def start_sampling(self, event):
		self.model.start_sampling()

	def stop_sampling(self, event):
		self.model.stop_sampling()

	def turn_on_pump(self, event):
		pump_name = event.GetEventObject().Name
		self.model.turn_on_pump(pump_name)

	def turn_off_pump(self, event):
		pump_name = event.GetEventObject().Name
		self.model.turn_off_pump(pump_name)

	def set_sampling_rate(self, event):
		sampling_rate = event.GetEventObject().GetValue()
		self.model.set_sampling_rate(sampling_rate)

	def set_setpoint(self, event):
		pump_name = event.GetEventObject().Name
		rpm = event.GetEventObject().GetValue()
		self.model.set_pump_speed_setpoint(pump_name, rpm)

	def on_close(self, event):
		# stop the sampling if running
		try:			
			self.model.stop_sampling()
		except:
			pass

		# save position and close all views
		for view_name in self.view:
			try:
				window_position_config.add_section(view_name)  # for making initial init_file
			except:
				pass

			x_pos, y_pos = self.view[view_name].GetPosition()
			window_position_config.set(view_name,'x_position', x_pos)
			window_position_config.set(view_name,'y_position', y_pos)
			self.view[view_name].Destroy()

		# writing the window positions to file
		config_file = open("./window_position.init",'w')
		window_position_config.write(config_file)
		config_file.close()



if __name__ == "__main__":
	app = wx.App(False)
	controller = Controller(app)
	app.MainLoop()

import os

class MyList(list):
    def longpop(self, start=0, length=1):
      	slice = self[start:start+length]
    	del self[start:start+length]
    	return slice

    @property
    def length(self):
        return len(self)

class DataStorage(object):
	def __init__(self, file_path='.', file_name='data_file.txt', store_length=40, remove_length=10):
		pass		
		self.store_length = store_length
		self.remove_length = remove_length

		self.data_writer = WriteDataToFile('.', 'test.txt')

	def add_data(self, new_data):
		try:
			for key in new_data:
				self.data[key].extend(new_data[key])
		except AttributeError:
			self.data = dict()
			for key in new_data:
				self.data[key] = MyList()
			self.data_writer.write_header(self.data)
			self.add_data(new_data)

		# writing older data to file
		if self.data[self.data.keys()[0]].length > self.store_length:
			self.data_writer.write_data(self.remove_data(self.remove_length))

	def get_data(self):
		return self.data

	def remove_data(self, remove_length):
		data_temp = dict()
		for key in self.data:
			data_temp[key] = self.data[key].longpop(length=remove_length)
		return data_temp

	def write_and_delete_data(self): 
	# intended for storing the rest of the data after closing the application
		print('removing and writing all data')
		self.data_writer.write_data(self.remove_data(self.data[self.data.keys()[0]].length))


class WriteDataToFile:

	def __init__(self, path_to_save_file, name_of_save_file):
		''' Create a save object '''
		self.path_to_save_file = path_to_save_file
		self.name_of_save_file = name_of_save_file

	def write_header(self, data):
		os.chdir(self.path_to_save_file)
		openfile = open(self.name_of_save_file,'a')
		
		header_string = ', '.join(data.keys())
		
		openfile.write(str(header_string))
		openfile.write('\n')
		openfile.close()
		

	def write_data(self, data):
		''' When called it will write the data (dict) to the file'''
		os.chdir(self.path_to_save_file)
		openfile = open(self.name_of_save_file,'a')

		tmp_list = list()

		for key in data:
			tmp_list.append((data[key]))
			
		tmp_list = zip(*tmp_list)
		
		
		for list_element in tmp_list:
			list_element = ', '.join(map(str, list_element)) #Separe data by commas
			openfile.write(str(list_element))
			openfile.write('\n')
		openfile.close()

if __name__ == '__main__':
	new_data = dict()
	new_data['data1'] = range(0,60)
	new_data['data2'] = range(0,60)
	print new_data.keys()[0]
	data = DataStorage()
	data.add_data(new_data)
	print(data.data)
	data.add_data(new_data)
	print(data.data)
	data.write_and_delete_data()




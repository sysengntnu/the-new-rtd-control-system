# Model implementation

from adam_modules import Adam4117
from adam_modules.utils import SimpleEventTimer
from adam_modules.utils import scan_ports
from adam_modules.utils import DataGenerator
from data_storage import DataStorage
from mac_motor_module.mactalk import Mac050
import time
from pubsub import pub



class Model(object):
    def __init__(self, sampling_rate=1):


        available_ports = list(scan_ports())
        print '* free ports:', available_ports

        # Trying to connect to the equipment one of the available ports
        for port in available_ports:
            try:
                self.voltage_sensor = Adam4117(port, 4)  # set up communication with Adam module
                self.voltage_sensor.get_name()
                print('* Found Adam modules on port: ' + str(port) + '  \(^v^)/')
                if not self.voltage_sensor.is_correct_module():
                    print '! It looks like the wrong class is used for the adam module (;v;)'
                break
            except:
                print('! Could not find the Adam modules on port: ' + str(port))

        # set up communication with the pump 
        self.pump = dict()
        self.pump['pump_1'] = Mac050(self.voltage_sensor.serial, 1)
        self.pump['pump_2'] = Mac050(self.voltage_sensor.serial, 2)
        #self.pump.append(Mac050(self.voltage_sensor.serial, 3)

        self.time_of_last_samle = time.time()

        # setting up data storage
        self.data = DataStorage(store_length=20, remove_length=5)
        self.rand_data = DataGenerator([10, 12, 13, 16, 18, 19, 20, 22, 26],[.5, .5, .5, .5, .5, .5, .5, .5, .5])
        self.sampling_rate = sampling_rate
        
    def start_sampling(self):

        # set up timer function
        self.loop = SimpleEventTimer(self.sample_system, self.sampling_rate)

        self.time_of_first_sample = time.time()
        self.sampling_error = 0.
        pub.sendMessage('sampling_status_changed', sampling_status='started')
        self.loop.start()

    def stop_sampling(self):
        self.loop.stop()
        self.data.write_and_delete_data()
        pub.sendMessage('sampling_status_changed', sampling_status='stopped')

    def set_sampling_rate(self, sampling_rate):
        self.sampling_rate = sampling_rate

    # get voltage signals
    def get_voltage(self, channel):
        return self.voltage_sensor.get_analog_in(channel)

    # Pump operations
    def turn_on_pump(self, pump_name):
        self.pump[pump_name].set_motormode(1)
        pub.sendMessage('pump_status_changed', pump_name=pump_name, pump_status='on')

    def turn_off_pump(self, pump_name):
        self.pump[pump_name].set_motormode(0)
        pub.sendMessage('pump_status_changed', pump_name=pump_name, pump_status='off')

    def set_pump_speed_setpoint(self, pump_name, speed_rpm):
        self.pump[pump_name].set_velocity(speed_rpm)
        pub.sendMessage('pump_setpoint_changed', pump_name=pump_name, setpoint_speed=speed_rpm)

    def get_pump_speed_setpoint(self, pump_name):
        return self.pump[pump_name].get_setvelocity()

    def get_pump_speed(self, pump_name):
        return self.pump[pump_name].get_actualvelocity()

    def get_stored_data(self):
        return self.data.get_data()

    # samples the system at given time intervals
    def sample_system(self):

        measurements = dict()

        for channel in range(0,2):
            measurements['voltage_' + str(channel)] = [self.get_voltage(channel)]

        for pump in self.pump:
            measurements[pump] = [self.get_pump_speed(pump)]
        
        measurements['time'] = [time.strftime('%H:%M:%S')]  # record time of sampling

        rand_data = self.rand_data.new_data()
        for channel in range(0,9):
            measurements['temperature_' + str(channel+1)] =  [rand_data[channel]]

        # calculate time since last sampling
        time_of_current_sample = time.time()
        time_since_last_sample = time_of_current_sample - self.time_of_last_samle

        self.sampling_error = time_since_last_sample - self.sampling_rate

        time_since_start = time_of_current_sample - self.time_of_first_sample

        measurements['time_since_start'] = [time_since_start]

        self.data.add_data(measurements)

        # send a message to anyone listening with newest sampling
        pub.sendMessage('sampled', measurements=measurements)


        print time_since_last_sample
        self.time_of_last_samle = time_of_current_sample


if __name__ == '__main__':
    def print_measurement(measurements):
        print measurements
        
    pub.subscribe(print_measurement,'sampled')
    model=Model()
    model.start_sampling()
    model.turn_on_pump('pump_1')
    model.turn_on_pump('pump_2')
    #model.turn_on_pump(2)
    model.set_pump_speed_setpoint('pump_1', 2)
    model.sample_system()
    model.turn_off_pump('pump_1')
    model.turn_off_pump('pump_2')
    print "test"
    time.sleep(2)
    print "test2"
    time.sleep(30)
    #model.turn_off_pump(2)
    model.sample_system()
    print model.voltage_sensor.get_name()
    print model.voltage_sensor.is_correct_module()
    model.stop_sampling()
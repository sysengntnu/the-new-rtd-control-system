import wx

# from FGB_module import FiberBraggGratingUnit

# # from value_temperature import ValueTemperature
# fiber = FiberBraggGratingUnit(1)
# temperature, wavelength, strain = fiber.perform_measurement()
# x = temperature


class ViewPump(wx.Frame):

	""" GUI for the pumps """

	def __init__(self, parent, title, *args, **kw):
		super(ViewPump, self).__init__(parent, title= 'Pump',size=(900,500),*args, **kw)


		self.InitUI()
		self.Centre()
		self.Show()
		
		#self.sample_timer.Bind(wx.EVT_TIMER, self.sample)
		#self.sample_timer.Start(100)

	def InitUI(self):

		self.panel = wx.Panel(self)

		self.sizer = wx.GridBagSizer(20, 20)

		"""GUI for the pumps"""

		# Create 3 buttons, one for each pump
		self.pump_1 = wx.ToggleButton(self.panel, label= 'Pump 1')
		self.pump_2 = wx.ToggleButton(self.panel, label= 'Pump 2')
		self.pump_3 = wx.ToggleButton(self.panel, label= 'Pump 3')

		# Add on our grid and give a position on the grid
		self.sizer.Add(self.pump_1, pos=(0,0), border= 10, flag= wx.ALIGN_CENTRE)
		self.sizer.Add(self.pump_2, pos=(0,1), border= 10, flag= wx.ALIGN_CENTRE)
		self.sizer.Add(self.pump_3, pos=(0,2), border= 10, flag= wx.ALIGN_CENTRE)

		# Bind this 3 button on functions for enable or disable them in function of what button is pressed
		self.pump_1.Bind(wx.EVT_TOGGLEBUTTON, self.toggle_pump_1)
		self.pump_2.Bind(wx.EVT_TOGGLEBUTTON, self.toggle_pump_2)
		self.pump_3.Bind(wx.EVT_TOGGLEBUTTON, self.toggle_pump_3)

		# Add a label for the pump
		self.label_name = wx.StaticText(self.panel, label= self.pump())
		self.sizer.Add(self.label_name, pos= (1, 0), border= 10, flag= wx.ALIGN_CENTRE)
		
		# Create a text area for display the title 'SetPoint' and add a Spincontrol for choose the speed of the pump
		self.set_point = wx.StaticText(self.panel, label= 'SetPoint')
		self.setpoint = wx.SpinCtrl(self.panel, value='1', size= (50, 25), min=1, max=2000)

		self.sizer.Add(self.set_point, pos =(2, 0), border= 10,  flag= wx.ALIGN_CENTRE)
		self.sizer.Add(self.setpoint, pos= (2, 1), border= 10, flag= wx.ALIGN_CENTRE)

		# Create a text area for display the title 'Speed' and add text area to display the actual speed of the pump
		self.speed = wx.StaticText(self.panel, label='Speed')
		self.speed1 = wx.StaticText(self.panel, label='Here, there is a value!')

		self.sizer.Add(self.speed, pos= (3,0), border= 10,  flag= wx.ALIGN_CENTRE)
		self.sizer.Add(self.speed1, pos= (3,1), border= 10, flag= wx.ALIGN_CENTRE)

		# Create 2 buttons, On and Off, for run or stop the pump which is choose
		self.on = wx.ToggleButton(self.panel, label='On', id=wx.ID_SAVE)
		self.off = wx.ToggleButton(self.panel, label='Off')

		self.sizer.Add(self.on, pos= (1, 1), flag= wx.ALIGN_CENTRE)
		self.sizer.Add(self.off, pos= (1, 2), flag= wx.ALIGN_CENTRE)

		self.on.Bind(wx.EVT_TOGGLEBUTTON, self.toggle_on)
		self.off.Bind(wx.EVT_TOGGLEBUTTON, self.toggle_off)

		try:
			print self.off.SetValue(True)
			self.off.Disable()
		except:
			print 'on1'

		"""GUi for the temperature sensors"""

		self.label_sensor_name = []
		self.label_temperature_reading =[]

		for i in range(9):

			j = str(i)
			self.label_sensor_name.append('Sensors '+ j +': ')
			self.label_temperature_reading.append(j) 

			name = str(self.label_sensor_name[i])
			st1 = wx.StaticText(self.panel, label=name)
			self.sizer.Add(st1, pos= (i , 18), flag= wx.ALIGN_CENTRE, border= 10)

			value = str(self.label_temperature_reading[i])
			st2 = wx.StaticText(self.panel, label= value )
			self.sizer.Add(st2, pos= (i, 19), flag= wx.ALIGN_CENTRE, border= 10)

		"""GUI for the conductivity sensor"""

		# Create an area for the conductivity sensor
		heading = wx.StaticText(self.panel, label='Sensor')
		self.sizer.Add(heading, pos= (8,0), flag= wx.ALIGN_CENTRE, border= 10)
		st_sensor_name = wx.StaticText(self.panel, label='Example')
		self.sizer.Add(st_sensor_name, pos= (9, 0), flag= wx.ALIGN_CENTRE, border= 10)
		
		# Create an area for display the voltage of the conductivity sensor
		voltage = wx.StaticText(self.panel, label='Voltage')
		self.sizer.Add(voltage, pos= (8, 1), flag= wx.ALIGN_CENTRE, border= 10)
		tc1 = wx.TextCtrl(self.panel, size= (50, 25))
		self.sizer.Add(tc1, pos= (9, 1), flag= wx.ALIGN_CENTRE, border= 10)
	
		self.panel.SetSizer(self.sizer)

	"""Functions for the pumps"""

	def OnClose (self, event):
		self.Close(True)

	# Define the function the button On, disable it and enable Off
	def toggle_on(self, event):
		self.on.Disable()
		self.off.Enable()
		self.off.SetValue(False)
		print self.on.GetValue()

	# Define the function the button Off, disable it and enable On
	def toggle_off(self, event):
		self.off.Disable()
		self.on.Enable()
		self.on.SetValue(False)
		print self.on.GetValue()

	# Define the function the button Pump_1, disable it and enable Pump_2 and Pump_3
	def toggle_pump_1(self, event):
		self.pump_1.Disable()
		self.pump_2.Enable()
		self.pump_2.SetValue(False)
		self.pump_3.Enable()
		self.pump_3.SetValue(False)
		print self.pump_1.GetValue()
		self.label_name.SetLabel("Oh, this is very looooong!")
    		

	# Define the function the button Pump_2, disable it and enable Pump_1 and Pump_3
	def toggle_pump_2(self, event):
		self.pump_2.Disable()
		self.pump_1.Enable()
		self.pump_1.SetValue(False)
		self.pump_3.Enable()
		self.pump_3.SetValue(False)
		print self.pump_2.GetValue()


		

	# Define the function the button Pump 3, disable it and enable Pump_1 and Pump_2
	def toggle_pump_3(self, event):
		self.pump_3.Disable()
		self.pump_1.Enable()
		self.pump_1.SetValue(False)
		self.pump_2.Enable()
		self.pump_2.SetValue(False)
		print self.pump_3.GetValue()


	def pump(self):
		if self.toggle_pump_1(self) == True:
			pump = 'Pump 1'
		elif self.toggle_pump_2(self) == True:
			pump = 'Pump 2'
		elif self.toggle_pump_3(self) == True:
			pump = 'Pump 3'
		else:
			pump = 'Wouallez!!!'

		return pump
		
		
	def set_name(self, name='Pump name'):
		# self.label_name.SetLabel(name)
		pass

	def set_speed(self, speed=0):
		self.speed.SetLabel(str(speed))

	def set_speed_setpoint(self, setpoint=0):
		self.setpoint.SetValue(float(setpoint))

	"""Functions for the temperature sensors"""

	def set_name(self, sensor_number, sensor_name):
		pass

	def set_value(self, sensor_number, sensor_name):
		pass


def main():

	ex = wx.App()
	ViewPump(None, title='Pump')
	ex.MainLoop()

if __name__ == '__main__':
	main()

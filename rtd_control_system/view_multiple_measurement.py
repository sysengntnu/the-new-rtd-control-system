import wx

class ViewMultipleMeasuremnt(wx.Frame):

    def __init__(self, parent, sensor_names, sensor_labels, main_title='Sensors', secondary_title = '', *args, **kw):
        super(ViewMultipleMeasuremnt, self).__init__(
            parent, 
            title=main_title,
            size=wx.DefaultSize, 
            style=wx.NO_BORDER|wx.CLOSE_BOX, 
            *args, 
            **kw
            )

        self.title = secondary_title

        self.sensor_names = sensor_names
        self.sensor_labels = sensor_labels

        self.init_ui()
        self.Centre()

    def init_ui(self):       

        self.number_of_measurements = len(self.sensor_names)

        # adding panel for cross-platform appearance 
        self.panel = wx.Panel(self, wx.ID_ANY)

        # adding sizers
        top_sizer       = wx.BoxSizer(wx.VERTICAL)
        title_sizer     = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer      = wx.GridSizer(rows=self.number_of_measurements, cols=2, hgap=5, vgap=5)
        

        self.label_title = wx.StaticText(self.panel, label=self.title)

        # assigning the widgets to a dictionary for easy reference later
        self.label_name_sensor = dict()
        self.label_value_temperature = dict()

        for sensor_name in self.sensor_names:
            # adding GUI widgets
            self.label_name_sensor[sensor_name] = wx.StaticText(self.panel, label=self.sensor_labels[sensor_name]['label'])
            self.label_value_temperature[sensor_name] = wx.StaticText(self.panel, label=  'NaN')
                

       
        # arranging and sizing the widgets 
        # alignment of title
        title_sizer.Add(self.label_title, 0, wx.ALL, 5)


        # arrangement of sensor names and sensor reading
        for sensor_name in self.sensor_names:     
            grid_sizer.Add(self.label_name_sensor[sensor_name], 0, wx.ALL, 5)
            grid_sizer.Add(self.label_value_temperature[sensor_name], 0, wx.ALL|wx.EXPAND, 5)
        

        # overall arrangement of the panel
        top_sizer.Add(title_sizer, 0, wx.CENTER|wx.CENTER)
        top_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL|wx.EXPAND, 5)
        top_sizer.Add(grid_sizer, 0, wx.ALL|wx.EXPAND, 5)

 
        self.panel.SetSizerAndFit(top_sizer)
        top_sizer.Fit(self)    
        self.top_sizer = top_sizer

    def set_sensor_reading(self, sensor_name, reading):
        if sensor_name in self.sensor_names:
            self.label_value_temperature[sensor_name].SetLabel(self.format_value(reading)+str(self.sensor_labels[sensor_name]['unit']))
            self.resize_window()
        else:
            raise ValueError('sensor_name is not in this Windows list of sensors')  


    def set_multiple_sensor_reading(self, sensors_name_and_reading):
        '''
        set multiple senor readings in one go
        arg: expect a dict with sensor name as key and the numerical value argument
        '''

        for sensor_name in sensors_name_and_reading:
            try:
                self.label_value_temperature[sensor_name].SetLabel(self.format_value(sensors_name_and_reading[sensor_name]))
                self.resize_window()
            except:
                pass

    def set_sensor_label(self, sensor_name, sensor_label):
        if sensor_name in self.sensor_names:
            self.sensor_labels[sensor_name] = sensor_label
            self.label_name_sensor[sensor_name].SetLabel(str(sensor_label))
            self.resize_window()
        else:
            raise ValueError('sensor_name is not in this Windows list of sensors') 

    def resize_window(self):
        self.top_sizer.Fit(self)

    def format_value(self, value):
        '''formating the numerical input for display in the GUI'''
        try:
            rounded_value = round(value[0], 2)
            return str(rounded_value)
        except:
            return str('BaNaN')

if __name__ == '__main__':

    # the name list and label dictionary
    # unsure where this should be put

    SENSORS = list()
    SENSORS.append('sensor_1')
    SENSORS.append('sensor_2')
    SENSORS.append('sensor_3')

    SENSOR_LABEL = dict()
    SENSOR_LABEL['sensor_1'] = 'Sensor 1'
    SENSOR_LABEL['sensor_2'] = 'Sensor 2'
    SENSOR_LABEL['sensor_3'] = 'Sensor 3'


    ex = wx.App()
    view = ViewMultipleMeasuremnt(None, SENSORS, SENSOR_LABEL, main_title='Sensors', secondary_title = 'Temperature Sensors')
    view.Show()
    view.set_sensor_reading('sensor_2',3)
    reading = dict()
    reading['sensor_1'] = 3.5
    reading['sensor_2'] = 17.1350298
    view.set_multiple_sensor_reading(reading)
    view.set_sensor_reading('sensor_3', 93999.23455290)
    view.set_sensor_label('sensor_3','Bolla')
    ex.MainLoop()

    

import wx

class ViewPump(wx.Frame):

    def __init__(self, parent, main_title='Pump Controller', secondary_title='Pump', name='pump', *args, **kw):
        super(ViewPump, self).__init__(
            parent, 
            title=main_title,
            style=wx.SYSTEM_MENU|wx.CAPTION|wx.CLOSE_BOX, 
            *args, 
            **kw
            )
        self.title = secondary_title
        self.name = name
        self.InitUI()
		# self.Centre()

    def InitUI(self):

        # adding panel for cross-platform appearance 
        self.panel = wx.Panel(self, wx.ID_ANY)

        # adding sizers
        top_sizer       = wx.BoxSizer(wx.VERTICAL)
        title_sizer     = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer      = wx.GridSizer(rows=2, cols=2, hgap=5, vgap=5)
        input_sizer     = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer    = wx.BoxSizer(wx.HORIZONTAL)
        
        # adding GUI widgets
        self.label_name = wx.StaticText(self.panel, label=self.title)

        self.label_description_speed = wx.StaticText(self.panel, label='Speed [rpm]')
        self.label_speed = wx.StaticText(self.panel, label='0 rpm')

        self.label_setpoint = wx.StaticText(self.panel, label='Speed setpoint [rpm]')
        self.spin_setpoint = wx.SpinCtrlDouble(self.panel, value='0', initial=0, inc=1, min=0, max=2000, name=self.name)

        self.on = wx.ToggleButton(self.panel, label='On', name=self.name)
        self.off = wx.ToggleButton(self.panel, label='Off', name=self.name)

        # arranging and sizing the widgets 
        # alignment of title
        title_sizer.Add(self.label_name, 0, wx.ALL, 5)

        # arrangement of the pump setpoint and pump speed 
        grid_sizer.Add(self.label_setpoint, 0, wx.ALL, 5)
        grid_sizer.Add(self.spin_setpoint, 0, wx.ALL, 5)
        grid_sizer.Add(self.label_description_speed, 0, wx.ALL, 5)
        grid_sizer.Add(self.label_speed, 0, wx.ALL, 5)

        # arrangement of the on/off buttons
        button_sizer.Add(self.on, 0, wx.ALL, 5)
        button_sizer.Add(self.off, 0, wx.ALL, 5)

        # overall arrangement of the panel
        top_sizer.Add(title_sizer, 0, wx.CENTER)
        top_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL|wx.EXPAND, 5)
        top_sizer.Add(grid_sizer, 0, wx.ALL|wx.CENTER, 5)
        top_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL|wx.EXPAND, 5)
        top_sizer.Add(button_sizer, 0, wx.ALL|wx.CENTER, 5)

        # assigning the sizer to the panel
        self.panel.SetSizer(top_sizer)

        # fit the sizer to the panel
        top_sizer.Fit(self)

        # setting initial status of on/off buttons
        self.off.SetValue(True)
        self.off.Disable()

    def toggle_on(self):
    	self.on.Disable()
    	self.off.Enable()
    	self.off.SetValue(False)


    def toggle_off(self):   
    	self.off.Disable()
    	self.on.Enable()
    	self.on.SetValue(False)

     
    def set_title(self, name='Pump name'):
    	self.label_name.SetLabel(name)

    def set_speed(self, speed=0):
    	self.label_speed.SetLabel(str(speed))

    def set_speed_setpoint(self, setpoint=0):
    	self.spin_setpoint.SetValue(float(setpoint)) 

def main():
	ex = wx.App()
	view = ViewPump(None, title='Pump NO.: 1', name='pump_1')
	view.Show()
	ex.MainLoop()

if __name__ == '__main__':
	main()
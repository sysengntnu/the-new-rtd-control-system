import wx

class ViewSampleController(wx.Frame):

    def __init__(self, parent, main_title='Sample Controller', name='pump', *args, **kw):
        super(ViewSampleController, self).__init__(
            parent, 
            title=main_title,
            style=wx.SYSTEM_MENU|wx.CAPTION|wx.CLOSE_BOX, 
            *args, 
            **kw
            )
        self.name = name
        self.InitUI()
		# self.Centre()

    def InitUI(self):

        # adding panel for cross-platform appearance 
        self.panel = wx.Panel(self, wx.ID_ANY)

        # adding sizers
        top_sizer       = wx.BoxSizer(wx.VERTICAL)
        title_sizer     = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer      = wx.GridSizer(rows=1, cols=2, hgap=5, vgap=5)
        input_sizer   	= wx.BoxSizer(wx.HORIZONTAL)
        button_sizer	= wx.BoxSizer(wx.HORIZONTAL)

        # adding GUI widgets
        self.label_status = wx.StaticText(self.panel, label='Status: Idle')

        self.label_setpoint = wx.StaticText(self.panel, label='Sample rate: ')
        self.spin_sampling_rate = wx.SpinCtrlDouble(self.panel, value='0', initial=0, inc=.1, min=0, max=2000, name=self.name)


        self.on = wx.ToggleButton(self.panel, label='Start Sampling', name=self.name)
        self.off = wx.ToggleButton(self.panel, label='Stop Sampling', name=self.name)

        # arranging and sizing the widgets 
        # alignment of title
        title_sizer.Add(self.label_status, 0, wx.ALL, 5)

        # arrangement of the pump setpoint and pump speed 
        grid_sizer.Add(self.label_setpoint, 0, wx.ALL, 5)
        grid_sizer.Add(self.spin_sampling_rate, 0, wx.ALL, 5)

        # arrangement of the on/off buttons
        button_sizer.Add(self.on, 0, wx.ALL, 5)
        button_sizer.Add(self.off, 0, wx.ALL, 5)

        # overall arrangement of the panel
        top_sizer.Add(title_sizer, 0, wx.CENTER)
        top_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL|wx.EXPAND, 5)
        top_sizer.Add(grid_sizer, 0, wx.ALL|wx.CENTER, 5)
        top_sizer.Add(button_sizer, 0, wx.ALL|wx.CENTER, 5)
        #top_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL|wx.EXPAND, 5)

        # assigning the sizer to the panel
        self.panel.SetSizer(top_sizer)

        # fit the sizer to the panel
        top_sizer.Fit(self)

        # setting initial status of on/off buttons
        self.off.SetValue(True)
        self.off.Disable()

    def toggle_on(self):
    	self.on.Disable()
    	self.off.Enable()
    	self.off.SetValue(False)
        self.spin_sampling_rate.Disable()
        self.label_status.SetLabel('Status: Sampling')


    def toggle_off(self):   
    	self.off.Disable()
    	self.on.Enable()
    	self.on.SetValue(False)
        self.spin_sampling_rate.Enable()
        self.label_status.SetLabel('Status: Idle')
     
    def set_title(self, titel):
    	self.label_status.SetLabel(title)

    def set_sampling_rate(self, sampling_rate=0.0):
    	self.spin_sampling_rate.SetValue(float(sampling_rate)) 

def main():
	ex = wx.App()
	view = ViewSampleController(None, main_title='Sample Controller', name='samle_controller')
	view.Show()
	ex.MainLoop()

if __name__ == '__main__':
	main()
#!/usr/bin/env python
# a simple attempt to make a dynamic plot for logging  

import wx
import wxmplot
import numpy as np

class ViewStripChart(wx.Frame):

    def __init__(self, 
                 parent, 
                 title='Plot', 
                 xlabel = '',
                 ylabel = '',
                 ymin = 0.,
                 ymax = 1.,
                 plot_fields=[],
                 *args, 
                 **kw):
        
        super(ViewStripChart, self).__init__(
            parent, 
            title=title,
            *args, 
            **kw
            )
        
        self.plot_fields = plot_fields
        self.number_of_lines = len(plot_fields)
        self.first_time = True
        self.ymin = ymin
        self.ymax = ymax

        # setting up plot
        self.plot_panel = wxmplot.PlotPanel(self, size=(500, 350), dpi=100)



        self.plot_panel.messenger = self.write_message

        self.plot_panel.set_xlabel(xlabel)
        self.plot_panel.set_ylabel(ylabel)

        
        # adding sizer
        self.panel_sizer = wx.BoxSizer()
        self.panel_sizer.Add(self.plot_panel)

        # assigning the sizer to the panel
        self.SetSizer(self.panel_sizer)

        # fit the sizer to the panel

        self.Fit()

    def write_message(self, message, panel=0):
        pass

    def update_plot(self, x, data_dict):
        x = np.array(x)


        if self.first_time:
            for n in range(0, self.number_of_lines-1):
                y = np.array(data_dict[self.plot_fields[n]])
                self.plot_panel.oplot(x, y, draw=False, ymin=self.ymin, ymax=self.ymax)

            y = np.array(data_dict[self.plot_fields[self.number_of_lines-1]])
            self.plot_panel.oplot(x, y, draw=True, show_legend=False, grid=True, ymin=self.ymin, ymax=self.ymax)

            self.panel_sizer.Fit(self)
            self.first_time = False

        else:
            for n in range(0, self.number_of_lines-1):
                y = np.array(data_dict[self.plot_fields[n]])
                self.plot_panel.update_line(n, x, y, draw=False)

            y = np.array(data_dict[self.plot_fields[self.number_of_lines-1]])
            self.plot_panel.update_line(self.number_of_lines-1, x, y, draw=True)


        self.plot_panel.set_xylims([x.min(), x.max(), self.ymin, self.ymax])



if __name__ == '__main__':
    app = wx.App()

    plot = ViewStripChart(None)
    plot.Show()

    x = np.linspace(0., 20, 1000)
    y = 5*np.sin(4*x)/(x+6)
    plot.update_plot(x, y)
    

    app.MainLoop()

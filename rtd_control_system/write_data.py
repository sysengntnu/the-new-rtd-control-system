import os

class WriteDataToFile:

	def __init__(self, path_to_save_file, name_of_save_file):
		''' Create a save object '''
		self.path_to_save_file = path_to_save_file
		self.name_of_save_file = name_of_save_file

	def write_data(self, data):
		''' When called it will write the data (dict) to the file'''
		self.data = data
		os.chdir(self.path_to_save_file)
		openfile = open(self.name_of_save_file,'a')
		data_name = data.keys()

		tmp_list = []
		for n in data_name:
			tmp =[]
			tmp.append((data[n]))
			tmp = tmp[0] #Simple way to remove parenthesis
			tmp_list.append(tmp)
			
		tmp_list = zip(*tmp_list)
		
		
		for x in tmp_list:
			x = ', '.join(map(str, x)) #Separe data by commas
			openfile.write(str(x))
			openfile.write('\n')
		openfile.close()


'''
This is a test for SaveDataToFile
'''

import os    
dat = {}
dat['voltage_1'] = 1, 0.1, 0.2, 0.3
dat['voltage_2'] = 2, 0.3, 0.1, 0.3
dat['voltage_3'] = 3, 25, 63, 1
saveFile = WriteDataToFile('.', 'test_1.txt')
saveFile.write_data(dat)

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

setup(
	name='rtd_control_system',
	version='0.1.0.dev',
	author='Chriss Grimholt',
    author_email='grimholt@me',
	description='Controlsystem interface for the rtd experiment',
	url='https://bitbucket.org/grimholt/the-new-rtd-control-system.git',
    packages=['rtd_control_system'],
    scripts=['bin/run.py'],
	install_requires = ['adam_modules', 'wxpython']
	)
